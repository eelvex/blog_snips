size(8cm,0);
unitsize(1cm,1cm);
import geometry;
import patterns;



point A=(0,0), B=(4,1), C=(3,5); 
point P1=(2,1), P2=(1,3);
P1=(1,3);


//filldraw(A--P2--B--cycle,lightgreen);
//filldraw(A--P2--C--cycle,lightgreen);
//filldraw(B--P2--C--cycle,lightgreen);


//filldraw(A--P1--B--cycle,lightgreen);
//filldraw(A--P1--C--cycle,lightgreen);
//filldraw(B--P1--C--cycle,lightgreen);

//draw(A--P2,blue+dashed);
//draw(B--P2,blue+dashed);
//draw(C--P2,blue+dashed);


dot("$A(x_1,y_1)$",A,2W,blue);
dot("$B(x_2,y_2)$",B,blue);
dot("$C(x_3,y_3)$",C,blue);
//dot("$P'(x,y)$",P2,2W);

//label("$\textrm{E}(ABC) \neq \textrm{E}(ABP) + \textrm{E}(APC) + \textrm{E}(PBC)$",S);
add("hatchback",hatch(2.5mm,NW,lightblue+0.01));
add("redhatch",hatch(2.5mm,NE,lightred+0.01));
add("redhatch2",hatch(2.5mm,N,lightred+0.01));
filldraw(A--C--B--cycle,lightgreen);
filldraw(A--C--P1--cycle,pattern("redhatch"));
filldraw(A--B--P1--cycle,pattern("hatchback"));
filldraw(C--B--P1--cycle,pattern("redhatch2"));
draw(A--P1,red+dashed);
draw(B--P1,red+dashed);
draw(C--P1,red+dashed);


draw(A--B,blue);
draw(C--B,blue);
draw(A--C,blue);
dot(Label("$P(x,y)$"),P1,2W,red);


shipout(bbox(white,Fill));
